import { customAttribute, inject } from 'aurelia-framework';

@customAttribute('sui-dropdown')
@inject(Element)
export class SUIDropdownCustomAttribute {

    constructor(element) {
        this.element = element;
    }

    attached() {
        $(this.element).dropdown();
    }
}
