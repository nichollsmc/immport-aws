import { customAttribute, inject } from 'aurelia-framework';

@customAttribute('sui-accordion')
@inject(Element)
export class SUIAccordionCustomAttribute {

    constructor(element) {
        this.element = element;
    }

    attached() {
        $(this.element).accordion();
    }
}
