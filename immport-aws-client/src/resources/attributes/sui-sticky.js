import { customAttribute, inject } from 'aurelia-framework';

@customAttribute('sui-sticky')
@inject(Element)
export class SUIStickyCustomAttribute {

    constructor(element) {
        this.element = element;
    }

    attached() {
        $(this.element).sticky();
    }
}
