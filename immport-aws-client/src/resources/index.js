export * from './value-converters/capitalize';
export * from './value-converters/date-format';
export * from './value-converters/filter';
export * from './value-converters/json';
export * from './value-converters/order-by';
export * from './value-converters/relative-time';
export * from './value-converters/sanitize-html';

export function configure(config) {
    config.globalResources([
        // Attributes
        './attributes/sui-accordion',
        './attributes/sui-dropdown',
        './attributes/sui-sticky',

        // Elements

        // Value converters
        './value-converters/capitalize',
        './value-converters/date-format',
        './value-converters/filter',
        './value-converters/filter-by',
        './value-converters/json',
        './value-converters/order-by',
        './value-converters/relative-time',
        './value-converters/sanitize-html'
    ]);
}
