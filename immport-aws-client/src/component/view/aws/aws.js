import { inject, LogManager } from 'aurelia-framework';
import { Ec2Repository }      from '../../repository/aws/ec2-repository';
import { ElbRepository }      from '../../repository/aws/elb-repository';
import { RdsRepository }      from '../../repository/aws/rds-repository';
import { Route53Repository }  from '../../repository/aws/route53-repository';

const LOG = LogManager.getLogger('infrastructure-browser');

@inject(Ec2Repository, ElbRepository, RdsRepository, Route53Repository)
export class InfrastructureBrowser {

    constructor(ec2Repository, elbRepository, rdsRepository, route53Repository) {
        this.ec2Repository = ec2Repository;
        this.elbRepository = elbRepository;
        this.rdsRepository = rdsRepository;
        this.route53Repository = route53Repository;
    }

    activate(params, routeConfig) {
        this.routeConfig = routeConfig;

        LOG.info(`Fetching AWS resources...\n\n`);

        return Promise.all([
            this.route53Repository.listHostedZones().then(this.orderByName),
            this.describeVpcResources()
        ])
        .then(([route53, vpcs]) => {
            this.route53 = route53;
            this.vpcs = vpcs;

            LOG.debug(`\n\nroute53: ${JSON.stringify(this.route53, null, 2)}`);
            LOG.debug(`\n\nvpcs: ${JSON.stringify(this.vpcs, null, 2)}`);
        });
    }

    describeVpcResources() {
        return Promise.all([
            this.ec2Repository.describeVpcs(),
            this.ec2Repository.describeInstances(),
            this.elbRepository.describeLoadBalancers(),
            this.rdsRepository.describeDBInstances()
        ])
        .then(([vpcs, instances, loadBalancers, databases]) =>
            vpcs.map(vpc => {
                // Map EC2
                let ec2 = instances.filter(instance => instance.vpcId === vpc.id);

                if (Array.isArray(ec2) && ec2.length > 0) {
                    vpc.ec2 = ec2;
                }

                // Map ELB
                let elb = loadBalancers.filter(lb => lb.vpcId === vpc.id);

                if (Array.isArray(elb) && elb.length > 0) {
                    vpc.elb = elb;
                }

                // Map RDS
                let rds = databases.filter(database => database.vpcId === vpc.id);

                if (Array.isArray(rds) && rds.length > 0) {
                    vpc.rds = rds;
                }

                if (!vpc.name) {
                    vpc.name = `Unknown (${vpc.id})`;
                }

                return vpc
            })
            .reduce((acc, val) => acc.concat(val), []))
        .then(this.orderByName);
    }

    orderByName(collection) {
        return collection.sort((v1, v2) => {
            var v1Name = v1.name.toUpperCase();
            var v2Name = v2.name.toUpperCase();

            if (v1Name < v2Name) {
                return -1;
            }

            if (v1Name > v2Name) {
                return 1;
            }

            return 0;
        });
    }
}
