import { inject, bindable, LogManager } from 'aurelia-framework';

@inject(Element)
export class LayoutNavCustomElement {

    @bindable router;

    constructor(element) {
        this.element = element;
        this.logger = LogManager.getLogger('layout-nav');
    }

    attached() {
        this.logger.debug('Attached!');

        $(this.element)
            .find('.ui.main.menu')
			.each(el => this.logger.debug('Setting visibility for %o', el))
			.visibility({ type: 'fixed' });
    }
}
