import { LogManager } from 'aurelia-framework';

export class App {

    constructor() {
        this.LOG = LogManager.getLogger(this.constructor.name);
    }

    configureRouter(config, router) {
        this.router = router;
        config.title = 'Immport AWS';
        config.options.pushState = true;
        config.options.root = '/';

        config.map([
            { route: ['', 'aws'], name: 'aws', moduleId: 'component/view/aws/aws', nav: true, title: 'AWS' }
        ]);

        config.mapUnknownRoutes('component/view/not-found');
    }
}
