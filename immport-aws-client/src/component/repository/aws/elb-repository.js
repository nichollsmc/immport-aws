import { LogManager }                      from 'aurelia-framework';
import { ELBV2_CONFIG }                    from './aws-repository-config';
import { AwsRepositorySupport as support } from './aws-repository-support';
import * as queryFilter                    from './aws-repository-query-filter';
import AWS                                 from 'aws-sdk';
import jmespath                            from 'jmespath';

const LOG = LogManager.getLogger('elb-repository');

export class ElbRepository {

    constructor() {
        this.elb = new AWS.ELBv2(ELBV2_CONFIG);
    }

    describeLoadBalancers() {
        LOG.info('Fetching load balancers...');

        return support.fetch({
            service: this.elb,
            operation: 'describeLoadBalancers',
            resultHandler(result) {
                return jmespath.search(result, queryFilter.ELB_DESCRIBE_ELB_FILTER);
            }
        });
    }

    describeTargetGroups() {
        LOG.info('Fetching load balancer target groups...');

        return support.fetch({
            service: this.elb,
            operation: 'describeTargetGroups'
        });
    }
}
