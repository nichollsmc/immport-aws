import { LogManager } from 'aurelia-framework';

const LOG = LogManager.getLogger('aws-repository-support');

export class AwsRepositorySupport {

    static fetch(endpoint) {
        LOG.debug(`Invoking operation '${endpoint.operation}' for service '${endpoint.service.api.className}'...`);

        let operation =
            (typeof endpoint.parameters !== 'undefined')
                ? endpoint.service[endpoint.operation](endpoint.parameters)
                : endpoint.service[endpoint.operation]();

        let promise = operation.promise();

        return promise
                .then(result =>
                    (typeof endpoint.resultHandler !== 'undefined')
                        ? endpoint.resultHandler(result)
                        : result)
                .catch(error => {
                    LOG.error(error);
                    throw error;
                });
    }
}
