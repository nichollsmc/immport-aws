import { LogManager }                      from 'aurelia-framework';
import { EC2_CONFIG }                      from './aws-repository-config';
import { AwsRepositorySupport as support } from './aws-repository-support';
import * as queryFilter                    from './aws-repository-query-filter';
import AWS                                 from 'aws-sdk';
import jmespath                            from 'jmespath';

const LOG        = LogManager.getLogger('ec2-repository');
const PARAMETERS = { DryRun: false };

export class Ec2Repository {

    constructor() {
        this.ec2 = new AWS.EC2(EC2_CONFIG);
    }

    describeInstances() {
        LOG.info('Fetching instances...');

        return support.fetch({
            service: this.ec2,
            operation: 'describeInstances',
            parameters: PARAMETERS,
            resultHandler(result) {
                return jmespath.search(result, queryFilter.EC2_DESCRIBE_INSTANCES_FILTER);
            }
        });
    }

    describeVpcs() {
        LOG.info('Fetching VPCs...');

        return support.fetch({
            service: this.ec2,
            operation: 'describeVpcs',
            parameters: PARAMETERS,
            resultHandler(result) {
                return jmespath.search(result, queryFilter.EC2_DESCRIBE_VPCS_FILTER);
            }
        });
    }
}
