import { LogManager }                      from 'aurelia-framework';
import { ROUTE53_CONFIG }                  from './aws-repository-config';
import { AwsRepositorySupport as support } from './aws-repository-support';
import * as queryFilter                    from './aws-repository-query-filter';
import AWS                                 from 'aws-sdk';
import jmespath                            from 'jmespath';

const LOG = LogManager.getLogger('route53-repository');

export class Route53Repository {

    constructor() {
        this.route53 = new AWS.Route53(ROUTE53_CONFIG);
    }

    listHostedZones() {
        LOG.info('Fetching hosted zones...');

        return support.fetch({
            service: this.route53,
            operation: 'listHostedZonesByName',
            resultHandler(result) {
                let filteredResult = jmespath.search(result, queryFilter.ROUTE53_LIST_HOSTED_ZONES_FILTER);

                return filteredResult.map(hz => {
                    hz.id = hz.id.substring(hz.id.lastIndexOf('/') + 1);
                    hz.name = hz.name.slice(0, hz.name.length - 1);

                    return hz;
                });
            }
        });
    }
}
