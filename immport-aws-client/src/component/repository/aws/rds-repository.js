import { LogManager }                      from 'aurelia-framework';
import { RDS_CONFIG }                      from './aws-repository-config';
import { AwsRepositorySupport as support } from './aws-repository-support';
import * as queryFilter                    from './aws-repository-query-filter';
import AWS                                 from 'aws-sdk';
import jmespath                            from 'jmespath';

const LOG = LogManager.getLogger('rds-repository');

export class RdsRepository {

    constructor() {
        this.rds = new AWS.RDS(RDS_CONFIG);
    }

    describeDBInstances() {
        LOG.info('Fetching DB instances...');

        return support.fetch({
            service: this.rds,
            operation: 'describeDBInstances',
            resultHandler(result) {
                return jmespath.search(result, queryFilter.RDS_DESCRIBE_DB_INSTANCES_FILTER);
            }
        });
    }
}
