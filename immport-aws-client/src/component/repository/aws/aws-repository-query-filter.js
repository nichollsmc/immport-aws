// ========================================
// EC2
// ========================================

export const EC2_DESCRIBE_INSTANCES_FILTER =
`Reservations[].Instances[].{
    name: Tags[?Key==\`Name\`] | [0].Value,
    publicIp: NetworkInterfaces[].Association.PublicIp | [0],
    publicDnsName: NetworkInterfaces[].Association.PublicDnsName | [0],
    type: InstanceType,
    state: State.Name,
    vpcId: VpcId,
    zone: Placement.AvailabilityZone,
    blockDevices: BlockDeviceMappings,
    securityGroups: SecurityGroups
}`;

export const EC2_DESCRIBE_SECURITY_GROUPS_FILTER = '';

export const EC2_DESCRIBE_VPCS_FILTER =
`Vpcs[].{
    id: VpcId,
    name: Tags[?Key==\`Name\`] | [0].Value,
    state: State
}`;

// ========================================
// ELB
// ========================================

export const ELB_DESCRIBE_ELB_FILTER =
`LoadBalancers[].{
    id: CanonicalHostedZoneId,
    name: LoadBalancerName,
    dnsName: DNSName,
    type: Type,
    state: State.Code,
    vpcId: VpcId,
    securityGroups: SecurityGroups
}`;

// ========================================
// Route53
// ========================================

export const RDS_DESCRIBE_DB_INSTANCES_FILTER =
`DBInstances[].{
    name: DBInstanceIdentifier,
    vpcId: DBSubnetGroup.VpcId,
    engine: Engine,
    engineVersion: EngineVersion,
    class: DBInstanceClass
}`;

// ========================================
// RDS
// ========================================

export const ROUTE53_LIST_HOSTED_ZONES_FILTER =
`HostedZones[].{
    id: Id,
    name: Name,
    description: Config.Comment
}`;
