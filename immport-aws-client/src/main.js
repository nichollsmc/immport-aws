import { LogManager }      from "aurelia-framework";
import { ConsoleAppender } from "aurelia-logging-console";
import environment         from './environment';
import * as AWS            from 'aws-sdk';
import 'jquery';
import 'semantic-ui';

Promise.config({
    warnings: {
        wForgottenReturn: false
    }
});

LogManager.addAppender(new ConsoleAppender());
LogManager.setLevel(LogManager.logLevel.info);

export function configure(aurelia) {
    aurelia.use
        .standardConfiguration()
        .plugin('aurelia-animator-css')
        .feature('resources');

    if (environment.debug) {
        LogManager.setLevel(LogManager.logLevel.debug);
    }

    if (environment.testing) {
        aurelia.use.plugin('aurelia-testing');
    }

    aurelia.start().then(() => aurelia.setRoot('component/application'));
}
