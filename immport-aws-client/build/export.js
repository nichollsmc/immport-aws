// this file provides a list of unbundled files that
// need to be included when exporting the application
// for production.
module.exports = {
    'list': [
        'index.html',
        'config.js',
        'favicon.ico',
        'LICENSE',
        'jspm_packages/system.js',
        'jspm_packages/system-polyfills.js',
        'jspm_packages/system-csp-production.js',
        'jspm_packages/github/systemjs/**/*',
        'styles/**/*',
        'assets/**/*'
    ],
    // this section lists any jspm packages that have
    // unbundled resources that need to be exported.
    // these files are in versioned folders and thus
    // must be 'normalized' by jspm to get the proper
    // path.
    'normalize': [
        [
            'bluebird', [
                '/js/browser/bluebird.min.js'
            ]
        ],
        [
            'jquery', [
                '/dist/jquery.min.js'
            ]
        ],
        [
            'dompurify', [
                '/dist/purify.min.js'
            ]
        ],
        [
            'moment', [
                '/locale/*',
                '/moment.js'
            ]
        ],
        [
            'numeral', [
                '/min/locales/*',
                '/min/locales.min.js',
                '/min/numeral.min.js'
            ]
        ],
        [
            'semantic-ui', [
                '/components/*',
                '/themes/**',
                '/semantic.min.css',
                '/semantic.min.js'
            ]
        ]
    ]
};
